import uvicorn
from fastapi import FastAPI

from recipte_app import models
from recipte_app.database import fixtures
from recipte_app.database.database import engine, session
from recipte_app.database.utils import fill_db, generate_db
from recipte_app.routers.recipe import router

app = FastAPI()

MODELS = (
    models.Ingredient,
    models.Recipe,
    models.IngredientRecipe,
)

FIXTURES = (
    fixtures.INGREDIENTS,
    fixtures.RECIPES,
    fixtures.INGREDIENTS_RECIPES,
)


@app.on_event("startup")
async def startup() -> None:
    await generate_db()
    await fill_db(MODELS, FIXTURES)


@app.on_event("shutdown")
async def shutdown() -> None:
    await session.close()
    await engine.dispose()


app.include_router(router)

if __name__ == "__main__":
    uvicorn.run("main:app")
