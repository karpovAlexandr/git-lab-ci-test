import typing

from fastapi import APIRouter

from recipte_app.models.recipe import Recipe
from recipte_app.schemas import recipe

router = APIRouter()


@router.get("/", response_model=typing.List[recipe.RecipeListOut])
async def recipes() -> typing.List[Recipe]:
    return await Recipe.get_all()


@router.get("/{pk}", response_model=recipe.RecipeOut)
async def recipe_detail(pk: int) -> Recipe:
    return await Recipe.get_and_increase_view(recipe_id=pk)
