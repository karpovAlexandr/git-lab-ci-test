from pydantic import BaseModel, Field


class IngredientBase(BaseModel):
    name: str = Field(str, title="Ingredient's name")


class IngredientIn(IngredientBase):
    ...


class IngredientOut(IngredientBase):
    class Config:
        orm_mode = True


class IngredientDetailOut(IngredientBase):
    id: int = Field(int, title="Ingredient's id")

    class Config:
        orm_mode = True
