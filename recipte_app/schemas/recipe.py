import typing

from pydantic import BaseModel, Field

from recipte_app.schemas.ingredient import IngredientOut


class RecipeBase(BaseModel):
    name: str = Field(str, title="Recipe's name")
    cooking_time: int = Field(int, title="Recipe's cooking time")


class RecipeListOut(RecipeBase):
    views: int = Field(int, title="Recipe's views")

    class Config:
        orm_mode = True


class RecipeDetailIn(RecipeBase):
    ingredients: typing.List[IngredientOut] = Field(
        typing.List[IngredientOut], title="Recipe's ingredients"
    )
    description: str = Field(str, title="Description's name")


class RecipeOut(RecipeDetailIn):
    class Config:
        orm_mode = True
