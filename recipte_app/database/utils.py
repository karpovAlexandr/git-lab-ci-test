from .database import Base, engine, session


async def fill_db(_models: tuple, _fixtures: tuple) -> None:
    async with session.begin():
        for model, model_objs in zip(_models, _fixtures):
            [session.add(model(**item)) for item in model_objs]


async def __generate_db() -> None:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


async def generate_db() -> None:
    await __generate_db()
