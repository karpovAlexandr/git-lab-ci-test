INGREDIENTS = [
    {"id": 1, "name": "carrot"},
    {"id": 2, "name": "eggs"},
    {"id": 3, "name": "apple"},
    {"id": 4, "name": "milk"},
    {"id": 5, "name": "flour"},
    {"id": 6, "name": "sugar"},
    {"id": 7, "name": "salt"},
]

RECIPES = [
    {
        "id": 1,
        "name": "Apple Cake",
        "cooking_time": 30,
        "description": "Tasty apple cake without sugar",
    }
]

INGREDIENTS_RECIPES = [
    {"recipe_id": 1, "ingredient_id": 2},
    {"recipe_id": 1, "ingredient_id": 3},
    {"recipe_id": 1, "ingredient_id": 4},
    {"recipe_id": 1, "ingredient_id": 5},
    {"recipe_id": 1, "ingredient_id": 7},
]
