from sqlalchemy import Column, ForeignKey, Integer

from recipte_app.database.database import Base


class IngredientRecipe(Base):
    __tablename__ = "ingredient_recipe"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True)
    ingredient_id = Column(Integer, ForeignKey("ingredient.id"))
    recipe_id = Column(Integer, ForeignKey("recipe.id"))

    def __repr__(self) -> str:
        return f"Recipe: {self.recipe.name}, Ingredient: {self.ingredient.name}"
