from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import RelationshipProperty, relationship

from recipte_app.database.database import Base


class Ingredient(Base):
    __tablename__ = "ingredient"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    recipes: "RelationshipProperty" = relationship(
        "Recipe", secondary="ingredient_recipe", back_populates="ingredients"
    )

    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self) -> str:
        return f"Ingredient: {self.name}"
