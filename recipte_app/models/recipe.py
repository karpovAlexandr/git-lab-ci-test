import typing

from sqlalchemy import Column, Integer, String
from sqlalchemy.future import select
from sqlalchemy.orm import RelationshipProperty, relationship, selectinload

from recipte_app.database.database import Base, session


class Recipe(Base):
    __tablename__ = "recipe"
    __table_args__ = {"extend_existing": True}

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    cooking_time = Column(Integer, index=True)
    description = Column(String)
    views = Column(Integer, default=0)
    ingredients: "RelationshipProperty" = relationship(
        "Ingredient", secondary="ingredient_recipe", back_populates="recipes"
    )

    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self) -> str:
        return f"Recipe: {self.name}"

    @classmethod
    async def get_all(cls) -> typing.List["Recipe"]:
        result = await session.execute(
            select(Recipe)
            .order_by(Recipe.views, Recipe.cooking_time)
            .options(selectinload(Recipe.ingredients))
        )
        return result.scalars().all()

    @staticmethod
    async def get_recipe(recipe_id: int) -> "Recipe":
        # https://ahmed-nafies.medium.com/sqlalchemy-async-orm-is-finally-here-d560dfaa335d
        # https://github.com/sqlalchemy/sqlalchemy/discussions/7835
        query = (
            select(Recipe)
            .where(Recipe.id == recipe_id)
            .options(selectinload(Recipe.ingredients))
        )
        results = await session.execute(query)
        (result,) = results.one()
        return result

    @classmethod
    async def get_and_increase_view(cls, recipe_id: int) -> "Recipe":
        recipe = await cls.get_recipe(recipe_id)
        recipe.views += 1
        session.add(recipe)
        await session.commit()
        return recipe
