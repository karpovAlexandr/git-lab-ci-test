source venv/bin/activate

echo "Units:"
pytest recipte_app/tests

echo "Mypy:"
mypy .
echo "Black:"
black --diff --check .

echo "isort:"
isort -- check-only --diff --profile black .

echo "flake8:"
flake8 .