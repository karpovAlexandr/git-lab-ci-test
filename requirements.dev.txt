black==22.10.0
flake8==5.0.4
flake8-bugbear==22.10.27
flake8-pie==0.16.0
isort==5.10.1
mypy==0.982